const sql = require("./db.js");


const ToDo = function (todo) {
  this.name = todo.name;
  this.status = todo.status;
};

ToDo.create = (newData, result) => {
  sql.query("INSERT INTO t_todo_list SET ?", newData, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    result(null, { id: res.insertId, ...newData });
  });
};

ToDo.updateById = (id, todo, result) => {
  sql.query(
    "UPDATE t_todo_list SET name = ?, status = ?, modify_date = ? WHERE id = ?",
    [
      todo.name,
      todo.status,
      new Date().toISOString().slice(0, 19).replace("T", " "),
      id,
    ],
    (err, res) => {
      if (err) {
        result(null, err);
        return;
      }
      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      result(null, { id: id, ...todo });
    }
  );
};

ToDo.getAll = (req, result) => {
  const status = req.query.status;
  console.log(status)
  let query = "SELECT * FROM t_todo_list";
  if (status) {
    query += ` WHERE status = '${status}'`;
  }
  sql.query(query, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  });
};

module.exports = ToDo;
