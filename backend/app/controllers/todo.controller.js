const ToDo = require("../models/todo.model.js");

exports.create = (req, res) => {
  // Validate request
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
  }
  // Create a object ToDo
  const todo = new ToDo({
    name: req.body.name,
    status: "0",
  });
  // Save ToDo in the database
  ToDo.create(todo, (err, data) => {
    if (err)
      res.status(500).send({
        status: 0,
        message: err.message || "Some error occurred while creating data.",
      });
    else
      res.send({
        status: 1,
        message: "success",
        data: data,
      });
  });
};

exports.update = (req, res) => {
  // Validate Request
  if (!req.body) {
    res.status(400).send({
      status: 0,
      message: "Content can not be empty!",
    });
  }
  // Update ToDo in the database
  ToDo.updateById(req.params.id, new ToDo(req.body), (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found id ${req.params.id}.`,
        });
      } else {
        res.status(500).send({
          status: 0,
          message: "Error updating with id " + req.params.id,
        });
      }
    } else
      res.send({
        status: 1,
        message: "success",
        data: data,
      });
  });
};

exports.findAll = (req, res) => {
  // Get All ToDo in the database
  ToDo.getAll(req, (err, data) => {
    if (err)
      res.status(500).send({
        status: 0,
        message: err.message || "Some error occurred while retrieving data.",
      });
    else
      res.send({
        status: 1,
        message: "success",
        data: data,
      });
  });
};
