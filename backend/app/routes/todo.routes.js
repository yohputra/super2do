module.exports = app => {
    const todo = require("../controllers/todo.controller.js");
    var router = require("express").Router();

    // Create a new To Do
    router.post("/create", todo.create);
    
    // Retrieve all To Do
    router.get("/all", todo.findAll);

    // Update a To do with id
    router.put("/update/:id", todo.update);
    
    app.use('/api', router);
  };